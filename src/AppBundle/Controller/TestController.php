<?php 

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Service\FlexService;

class TestController extends Controller {

    /**
     * @Route("/test")
     */
    public function numberAction()
    {
        $number = mt_rand(0, 100);

        // return new Response(
        //     '<html><body><b>Lucky number:</b> '.$number.'</body></html>'
        // );

        return $this->render('testView/luckyView.html.twig', array('number'=>$number));
    }

    /**
     * @Route("/awsrandom", name="aws_random") #, options={"expose"=True}, methods={"GET,POST"})
     */
    public function awsRandom(Request $request)
    {
        $response = ['status' => 'error', 'result' => null];

        $number = mt_rand(0, 100);
        $response['result'] = $number;

        return new JsonResponse($response);
    }

    /**
     * @Route("/getList", name="aws_random") #, options={"expose"=True}, methods={"GET,POST"})
     */
    public function getList(Request $request)
    {
        $response = ['status' => 'error', 'result' => null];

        $em = $this->getDoctrine()->getManager();
        try {
            $query = $em->getRepository('AppBundle:Table1')
                    ->createQueryBuilder('t1')
                ;

            $entities = $query->getQuery()->getArrayResult();
            if ($entities) {
                $count_entities = count($entities);
                
                $page = 1;
                $limit = 10;

                $query->setMaxResults($limit);
                $query->setFirstResult(($page * $limit) - $limit);
                $entities = $query->getQuery()->getResult();

                $entities = FlexService::convertToJSON($entities);

                $response['result']['count'] = $count_entities;
                $response['result']['data'] = $entities;
                
            }

            $response = $entities; //['result'] = 'success';
            
        }
        catch (\Exception $e) {
            $response['result'] = $e->getMessage();
        }

        return new JsonResponse($response);
    }

        /**
     * @Route("/getData", name="aws_random") #, options={"expose"=True}, methods={"GET,POST"})
     */
    public function getData(Request $request)
    {
        $repository = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Table1');
        
        $query = $repository->createQueryBuilder('t1')
                ->getQuery()
                ;

        $dat = $query->getResult();
        $dat = FlexService::convertToJSON($dat);
        // $dat->headers->set('Access-Control-Allow-Origin', '*');


        return new JsonResponse($dat);
        
    }


    /**
     * @Route("/api", name="aws_get_list") #, options={"expose"=True}, methods={"GET"})     
     */
    public function apiAction(){
        // https://ourcodeworld.com/articles/read/291/how-to-solve-the-client-side-access-control-allow-origin-request-error-with-your-own-symfony-3-api
        // $response = ['status' => mt_rand(0, 100), 'title' => uniqid()];
        $response = new Response();
        
        $date = new \DateTime();

        
        // $response = FlexService::convertToJSON($response);
        $response->setContent(json_encode(['id' => mt_rand(0, 100),'title' => uniqid()]));

        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
       
        // return new JsonResponse($response);
        return $response;
    }

}

?>