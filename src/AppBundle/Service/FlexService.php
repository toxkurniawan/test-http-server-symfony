<?php

namespace AppBundle\Service;

use AppBundle\Entity\AccountBalance;
use AppBundle\Entity\JournalEntryList;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class FlexService
{
    public static function getFlexSerializer() {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setIgnoredAttributes(["__initializer__", "__cloner__","__isInitialized__"]);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return null;
        });

        // Format DateTime
        $callback = function ($dateTime) {
            return $dateTime instanceof \DateTime
                ? $dateTime->format(\DateTime::ISO8601)
                : '';
        };
        $normalizer->setCallbacks(array(
            'postingDate' => $callback,
            'docDate' => $callback,
            'dueDate' => $callback,
            'deliveryDate' => $callback,
            'validFrom' => $callback,
            'validTo' => $callback,
            'createdAt' => $callback,
            'updatedAt' => $callback,
            'effectiveDate' => $callback,
            'expirationDate' => $callback,
            'invoiceDate' => $callback,
            'dueDateTime' => $callback,
            'paymDateTime' => $callback
        ));

        $serializer = new Serializer(array($normalizer), array($encoder));

        return $serializer;
    }

    public static function getMultipleDiscAmount($arrDiscount)
    {
        $discAmount = 0;
        if (!empty($arrDiscount)) {
            $n = 1;
            foreach ($arrDiscount as $key => $value) {
                $n *= 1 - ($value / 100);
            }

            $discAmount = (1 - $n) * 100;
        }

        return $discAmount;
    }

    public static function getDecreaseDiscAmount($discTotal, $discount)
    {
        $discAmount = 0;
        if ($discTotal != null && $discount != null) {
            $discTotal = 1 - ($discTotal / 100);
            $discount = 1 - ($discount / 100);
            $discAmount = (1 - ($discTotal / $discount)) * 100;
        }

        return $discAmount;
    }

    public static function convertToJSON($object)
    {
        $serializer = self::getFlexSerializer();

        $object = $serializer->serialize($object, 'json');
        $object = json_decode($object, true);

        return $object;
    }

    public static function generateBarcode($id)
    {
        $date = new \DateTime();
        $length = 9;

        $zeroPad = str_pad($id, $length, '0', STR_PAD_LEFT);
        $barcode = $date->format('ym') . $zeroPad;

        return $barcode;
    }

    public static function getDocumentSeries($em, $documentId, $seriesId)
    {
        $documentNumbering = $em->getRepository('AppBundle:DocumentNumbering')->findOneByDocumentid($documentId);
        if ($documentNumbering) {
            $documentSeries = $em->getRepository('AppBundle:DocumentSeries')->findOneBy(array(
                'documentNumbering' => $documentNumbering,
                'seriesid' => $seriesId,
            ));

            if ($documentSeries) return $documentSeries;
        }

        return null;
    }

    public static function getPostingPeriod($em, $date)
    {
        $query = $em->createQuery(
            "SELECT postingPeriod
            FROM AppBundle:PostingPeriod postingPeriod
            WHERE postingPeriod.postingDateFrom <= :date AND
                  postingPeriod.postingDateTo >= :date"
        );
        $query->setParameter('date', $date);
        $postingPeriod = $query->getResult();

        if ($postingPeriod) return $postingPeriod[0];
        else return null;
    }

    public static function generateDocumentNumber($em, $id)
    {
        try {
            $documentSeries = $em->getRepository('AppBundle:DocumentSeries')->find($id);

            $seriesId = $documentSeries->getSeriesid();
            $firstNumber = $documentSeries->getFirstNumber();
            $nextNumber = intval($documentSeries->getNextNumber());
            $lastNumber = intval($documentSeries->getLastNumber());
            $lengthNumber = $documentSeries->getLengthNumber();
            $prefix = $documentSeries->getPrefix();
            $suffix = $documentSeries->getSuffix();

            $documentNumber = null;
            if ($seriesId != 'Manual' && $nextNumber > 0) {
                if ($nextNumber < $firstNumber) {
                    $nextNumber = $firstNumber;
                    $documentSeries->setNextNumber($nextNumber);
                    $em->flush($documentSeries);
                }

                if ($lastNumber == 0 || $lastNumber >= $nextNumber) {
                    $zeroPad = str_pad($nextNumber, intval($lengthNumber), "0", STR_PAD_LEFT);
                    $documentNumber = $prefix . $zeroPad . $suffix;
                    self::updateDocumentNumber($em, $id);
                }
                else return 'maximum';
            }

            return $documentNumber;
        }
        catch (\Exception $e) {
            return 'error';
        }
    }

    public static function updateDocumentNumber($em, $id)
    {
        $documentSeries = $em->getRepository('AppBundle:DocumentSeries')->find($id);
        if ($documentSeries) {
            $nextNumber = $documentSeries->getNextNumber();
            $nextNumber += 1;

            $documentSeries->setNextNumber($nextNumber);
            $em->flush($documentSeries);
        }
    }

    public static function insertJournalList($em, $journalEntry, $businessPartner,
                                      $debitAccount, $creditAccount, $amount,
                                      $taxGroup, $taxAmount, $remarks)
    {
        // Debit
        $journalEntryList = new JournalEntryList();
        $journalEntryList->setJournalEntry($journalEntry);
        $journalEntryList->setBusinessPartner($businessPartner);
        $journalEntryList->setCoa($debitAccount);
        $journalEntryList->setDebit($amount);
        $journalEntryList->setCredit(0);
        $journalEntryList->setTaxGroup($taxGroup);
        $journalEntryList->setTaxAmount($taxAmount);
        $journalEntryList->setRemarks($remarks);
        $em->persist($journalEntryList);
        $em->flush($journalEntryList);

        self::insertAccountBalance($em, $debitAccount, $journalEntry->getPostingDate(), $journalEntry->getPostingPeriod(), $journalEntry->getDocNumber(), $journalEntry->getDocType(),
            $journalEntry->getDocRefNo(), $businessPartner ? $businessPartner->getCode() : null, $amount, $remarks);

        // Credit
        $journalEntryList = new JournalEntryList();
        $journalEntryList->setJournalEntry($journalEntry);
        $journalEntryList->setBusinessPartner($businessPartner);
        $journalEntryList->setCoa($creditAccount);
        $journalEntryList->setDebit(0);
        $journalEntryList->setCredit($amount);
        $journalEntryList->setTaxGroup($taxGroup);
        $journalEntryList->setTaxAmount($taxAmount);
        $journalEntryList->setRemarks($remarks);
        $em->persist($journalEntryList);
        $em->flush($journalEntryList);

        self::insertAccountBalance($em, $creditAccount, $journalEntry->getPostingDate(), $journalEntry->getPostingPeriod(), $journalEntry->getDocNumber(), $journalEntry->getDocType(),
            $journalEntry->getDocRefNo(), $businessPartner ? $businessPartner->getCode() : null, $amount * -1, $remarks);


    }

    public static function insertAccountBalance($em, $coa, $postingDate, $postingPeriod, $docNumber, $docType, $docRefNo,
                                         $offsetAccount, $amount, $description)
    {
        $accountBalance = new AccountBalance();
        $accountBalance->setCoa($coa);
        $accountBalance->setPostingDate($postingDate);
        $accountBalance->setPostingPeriod($postingPeriod);
        $accountBalance->setDocNumber($docNumber);
        $accountBalance->setDocType($docType);
        $accountBalance->setDocRefNo($docRefNo);
        $accountBalance->setOffsetAccount($offsetAccount);
        $accountBalance->setBalanceLc($amount);
        $accountBalance->setBalanceSc($amount);
        $accountBalance->setBalanceFc(0);
        $accountBalance->setBalanceDueLc($amount);
        $accountBalance->setBalanceDueSc($amount);
        $accountBalance->setBalanceDueFc(0);
        $accountBalance->setDescription($description);
        $em->persist($accountBalance);
        $em->flush($accountBalance);

        // update opening balance on chart of account
        $coaBalance = floatval($coa->getCurrentBalance());
        $coaBalance += $amount;
        $coa->setCurrentBalance($coaBalance);
        $em->flush($coa);
    }
}