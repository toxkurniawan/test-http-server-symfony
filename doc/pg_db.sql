/*
Navicat PGSQL Data Transfer

Source Server         : AWS_Postgre
Source Server Version : 90306
Source Host           : 192.168.56.104:5432
Source Database       : dobuy2
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90306
File Encoding         : 65001

Date: 2018-08-20 14:39:46
*/


-- ----------------------------
-- Sequence structure for bank_account_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."bank_account_id_seq";
CREATE SEQUENCE "public"."bank_account_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for bank_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."bank_id_seq";
CREATE SEQUENCE "public"."bank_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for brand_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."brand_id_seq";
CREATE SEQUENCE "public"."brand_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for city_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."city_id_seq";
CREATE SEQUENCE "public"."city_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for color_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."color_id_seq";
CREATE SEQUENCE "public"."color_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for country_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."country_id_seq";
CREATE SEQUENCE "public"."country_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for coupon_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."coupon_id_seq";
CREATE SEQUENCE "public"."coupon_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for courier_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."courier_id_seq";
CREATE SEQUENCE "public"."courier_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for courier_service_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."courier_service_id_seq";
CREATE SEQUENCE "public"."courier_service_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for currency_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."currency_id_seq";
CREATE SEQUENCE "public"."currency_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for customer_address_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."customer_address_id_seq";
CREATE SEQUENCE "public"."customer_address_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for customer_bank_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."customer_bank_id_seq";
CREATE SEQUENCE "public"."customer_bank_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for customer_card_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."customer_card_id_seq";
CREATE SEQUENCE "public"."customer_card_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for customer_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."customer_id_seq";
CREATE SEQUENCE "public"."customer_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for customer_meta_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."customer_meta_id_seq";
CREATE SEQUENCE "public"."customer_meta_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for media_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."media_id_seq";
CREATE SEQUENCE "public"."media_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for media_meta_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."media_meta_id_seq";
CREATE SEQUENCE "public"."media_meta_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for merchant_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."merchant_id_seq";
CREATE SEQUENCE "public"."merchant_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for merchant_meta_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."merchant_meta_id_seq";
CREATE SEQUENCE "public"."merchant_meta_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for merchant_product_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."merchant_product_id_seq";
CREATE SEQUENCE "public"."merchant_product_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for payment_method_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."payment_method_id_seq";
CREATE SEQUENCE "public"."payment_method_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for product_attribute_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."product_attribute_id_seq";
CREATE SEQUENCE "public"."product_attribute_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for product_category_attribute_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."product_category_attribute_id_seq";
CREATE SEQUENCE "public"."product_category_attribute_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for product_category_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."product_category_id_seq";
CREATE SEQUENCE "public"."product_category_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for product_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."product_id_seq";
CREATE SEQUENCE "public"."product_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for product_image_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."product_image_id_seq";
CREATE SEQUENCE "public"."product_image_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for product_meta_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."product_meta_id_seq";
CREATE SEQUENCE "public"."product_meta_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for promo_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."promo_id_seq";
CREATE SEQUENCE "public"."promo_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for province_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."province_id_seq";
CREATE SEQUENCE "public"."province_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for system_language_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."system_language_id_seq";
CREATE SEQUENCE "public"."system_language_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for system_language_list_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."system_language_list_id_seq";
CREATE SEQUENCE "public"."system_language_list_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for system_log_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."system_log_id_seq";
CREATE SEQUENCE "public"."system_log_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for system_setting_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."system_setting_id_seq";
CREATE SEQUENCE "public"."system_setting_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for system_user_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."system_user_id_seq";
CREATE SEQUENCE "public"."system_user_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for system_user_meta_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."system_user_meta_id_seq";
CREATE SEQUENCE "public"."system_user_meta_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Table structure for bank
-- ----------------------------
DROP TABLE IF EXISTS "public"."bank";
CREATE TABLE "public"."bank" (
"id" int8 DEFAULT nextval('bank_id_seq'::regclass) NOT NULL,
"name" varchar(100) COLLATE "default" NOT NULL,
"country_id" int8 NOT NULL,
"media_id" int8,
"swiftcode" varchar(45) COLLATE "default",
"api_url" varchar(255) COLLATE "default",
"api_key" varchar(255) COLLATE "default",
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of bank
-- ----------------------------

-- ----------------------------
-- Table structure for bank_account
-- ----------------------------
DROP TABLE IF EXISTS "public"."bank_account";
CREATE TABLE "public"."bank_account" (
"id" int8 DEFAULT nextval('bank_account_id_seq'::regclass) NOT NULL,
"bank_id" int8 NOT NULL,
"account_no" varchar(45) COLLATE "default" NOT NULL,
"name" varchar(45) COLLATE "default" NOT NULL,
"branch" varchar(100) COLLATE "default",
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of bank_account
-- ----------------------------

-- ----------------------------
-- Table structure for brand
-- ----------------------------
DROP TABLE IF EXISTS "public"."brand";
CREATE TABLE "public"."brand" (
"id" int8 DEFAULT nextval('brand_id_seq'::regclass) NOT NULL,
"code" varchar(20) COLLATE "default" NOT NULL,
"name" varchar(100) COLLATE "default",
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of brand
-- ----------------------------

-- ----------------------------
-- Table structure for city
-- ----------------------------
DROP TABLE IF EXISTS "public"."city";
CREATE TABLE "public"."city" (
"id" int8 DEFAULT nextval('city_id_seq'::regclass) NOT NULL,
"name" varchar(45) COLLATE "default" NOT NULL,
"province_id" int8 NOT NULL,
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of city
-- ----------------------------

-- ----------------------------
-- Table structure for color
-- ----------------------------
DROP TABLE IF EXISTS "public"."color";
CREATE TABLE "public"."color" (
"id" int8 DEFAULT nextval('color_id_seq'::regclass) NOT NULL,
"name" varchar(100) COLLATE "default" NOT NULL,
"hexcode" varchar(20) COLLATE "default",
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of color
-- ----------------------------

-- ----------------------------
-- Table structure for country
-- ----------------------------
DROP TABLE IF EXISTS "public"."country";
CREATE TABLE "public"."country" (
"id" int8 DEFAULT nextval('country_id_seq'::regclass) NOT NULL,
"name" varchar(100) COLLATE "default" NOT NULL,
"intercode" varchar(10) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of country
-- ----------------------------

-- ----------------------------
-- Table structure for coupon
-- ----------------------------
DROP TABLE IF EXISTS "public"."coupon";
CREATE TABLE "public"."coupon" (
"id" int8 DEFAULT nextval('coupon_id_seq'::regclass) NOT NULL,
"code" varchar(45) COLLATE "default" NOT NULL,
"name" varchar(100) COLLATE "default",
"valid_from" date NOT NULL,
"valid_to" date NOT NULL,
"type" int2,
"value" numeric(19,5),
"minimum_purchase" numeric(19,5),
"media_id" int8,
"qty_available" numeric(19,5) NOT NULL,
"qty_used" numeric(19,5) NOT NULL,
"description" text COLLATE "default",
"enabled" bool NOT NULL,
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of coupon
-- ----------------------------

-- ----------------------------
-- Table structure for courier
-- ----------------------------
DROP TABLE IF EXISTS "public"."courier";
CREATE TABLE "public"."courier" (
"id" int8 DEFAULT nextval('courier_id_seq'::regclass) NOT NULL,
"name" varchar(100) COLLATE "default" NOT NULL,
"media_id" int8,
"api_url" varchar(255) COLLATE "default",
"api_key" varchar(255) COLLATE "default",
"enabled" bool NOT NULL,
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of courier
-- ----------------------------

-- ----------------------------
-- Table structure for courier_service
-- ----------------------------
DROP TABLE IF EXISTS "public"."courier_service";
CREATE TABLE "public"."courier_service" (
"id" int8 DEFAULT nextval('courier_service_id_seq'::regclass) NOT NULL,
"courier_id" int8 NOT NULL,
"name" varchar(100) COLLATE "default" NOT NULL,
"enabled" bool NOT NULL,
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of courier_service
-- ----------------------------

-- ----------------------------
-- Table structure for currency
-- ----------------------------
DROP TABLE IF EXISTS "public"."currency";
CREATE TABLE "public"."currency" (
"id" int8 DEFAULT nextval('currency_id_seq'::regclass) NOT NULL,
"code" varchar(10) COLLATE "default" NOT NULL,
"name" varchar(60) COLLATE "default" NOT NULL,
"intercode" varchar(10) COLLATE "default",
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of currency
-- ----------------------------

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS "public"."customer";
CREATE TABLE "public"."customer" (
"id" int8 DEFAULT nextval('customer_id_seq'::regclass) NOT NULL,
"name" varchar(100) COLLATE "default" NOT NULL,
"username" varchar(100) COLLATE "default" NOT NULL,
"birthday" date,
"gender" varchar(45) COLLATE "default",
"email" varchar(100) COLLATE "default",
"phone" varchar(20) COLLATE "default",
"total_point" numeric(19,5),
"total_balance" numeric(19,5),
"total_coupon" numeric(19,5),
"media_id" int8,
"system_user_id" int8 NOT NULL,
"enabled" bool NOT NULL,
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of customer
-- ----------------------------

-- ----------------------------
-- Table structure for customer_address
-- ----------------------------
DROP TABLE IF EXISTS "public"."customer_address";
CREATE TABLE "public"."customer_address" (
"id" int8 DEFAULT nextval('customer_address_id_seq'::regclass) NOT NULL,
"customer_id" int8 NOT NULL,
"label" varchar(100) COLLATE "default" NOT NULL,
"name" varchar(100) COLLATE "default" NOT NULL,
"address" text COLLATE "default",
"phone" varchar(20) COLLATE "default",
"email" varchar(100) COLLATE "default",
"country_id" int8 NOT NULL,
"province_id" int8 NOT NULL,
"city_id" int8 NOT NULL,
"postal_code" varchar(10) COLLATE "default",
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of customer_address
-- ----------------------------

-- ----------------------------
-- Table structure for customer_bank
-- ----------------------------
DROP TABLE IF EXISTS "public"."customer_bank";
CREATE TABLE "public"."customer_bank" (
"id" int8 DEFAULT nextval('customer_bank_id_seq'::regclass) NOT NULL,
"customer_id" int8 NOT NULL,
"account_no" varchar(45) COLLATE "default" NOT NULL,
"name" varchar(100) COLLATE "default" NOT NULL,
"bank_id" int8 NOT NULL,
"branch" varchar(100) COLLATE "default",
"otp_type" varchar(45) COLLATE "default",
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of customer_bank
-- ----------------------------

-- ----------------------------
-- Table structure for customer_card
-- ----------------------------
DROP TABLE IF EXISTS "public"."customer_card";
CREATE TABLE "public"."customer_card" (
"id" int8 DEFAULT nextval('customer_card_id_seq'::regclass) NOT NULL,
"customer_id" int8 NOT NULL,
"label" varchar(100) COLLATE "default" NOT NULL,
"card_no" varchar(45) COLLATE "default" NOT NULL,
"name" varchar(100) COLLATE "default" NOT NULL,
"valid_period_month" int2 NOT NULL,
"valid_period_year" int2 NOT NULL,
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of customer_card
-- ----------------------------

-- ----------------------------
-- Table structure for customer_meta
-- ----------------------------
DROP TABLE IF EXISTS "public"."customer_meta";
CREATE TABLE "public"."customer_meta" (
"id" int8 DEFAULT nextval('customer_meta_id_seq'::regclass) NOT NULL,
"customer_id" int8 NOT NULL,
"meta_key" varchar(255) COLLATE "default",
"meta_value" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of customer_meta
-- ----------------------------

-- ----------------------------
-- Table structure for media
-- ----------------------------
DROP TABLE IF EXISTS "public"."media";
CREATE TABLE "public"."media" (
"id" int8 DEFAULT nextval('media_id_seq'::regclass) NOT NULL,
"filename" varchar(255) COLLATE "default" NOT NULL,
"title" varchar(100) COLLATE "default",
"filetype" varchar(45) COLLATE "default",
"filesize" varchar(20) COLLATE "default",
"dimension" varchar(20) COLLATE "default",
"description" text COLLATE "default",
"path" varchar(255) COLLATE "default",
"category" varchar(45) COLLATE "default" NOT NULL,
"system_user_id" int8,
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of media
-- ----------------------------

-- ----------------------------
-- Table structure for media_meta
-- ----------------------------
DROP TABLE IF EXISTS "public"."media_meta";
CREATE TABLE "public"."media_meta" (
"id" int8 DEFAULT nextval('media_meta_id_seq'::regclass) NOT NULL,
"media_id" int8 NOT NULL,
"meta_key" varchar(255) COLLATE "default",
"meta_value" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of media_meta
-- ----------------------------

-- ----------------------------
-- Table structure for merchant
-- ----------------------------
DROP TABLE IF EXISTS "public"."merchant";
CREATE TABLE "public"."merchant" (
"id" int8 DEFAULT nextval('merchant_id_seq'::regclass) NOT NULL,
"name" varchar(100) COLLATE "default" NOT NULL,
"address" text COLLATE "default",
"country_id" int8 NOT NULL,
"province_id" int8 NOT NULL,
"city_id" int8 NOT NULL,
"postal_code" varchar(20) COLLATE "default",
"slogan" varchar(100) COLLATE "default",
"note" text COLLATE "default",
"type" int2 DEFAULT 0 NOT NULL,
"system_user_id" int8 NOT NULL,
"media_id" int8,
"suspend" bool NOT NULL,
"enabled" bool NOT NULL,
"slug" varchar(255) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."merchant"."type" IS '0 = Regular Seller, 1 = Gold Seller, 2 = Official Store';

-- ----------------------------
-- Records of merchant
-- ----------------------------

-- ----------------------------
-- Table structure for merchant_meta
-- ----------------------------
DROP TABLE IF EXISTS "public"."merchant_meta";
CREATE TABLE "public"."merchant_meta" (
"id" int8 DEFAULT nextval('merchant_meta_id_seq'::regclass) NOT NULL,
"merchant_id" int8 NOT NULL,
"meta_key" varchar(255) COLLATE "default",
"meta_value" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of merchant_meta
-- ----------------------------

-- ----------------------------
-- Table structure for merchant_product
-- ----------------------------
DROP TABLE IF EXISTS "public"."merchant_product";
CREATE TABLE "public"."merchant_product" (
"id" int8 DEFAULT nextval('merchant_product_id_seq'::regclass) NOT NULL,
"merchant_id" int8 NOT NULL,
"merchant_storage_id" int8 NOT NULL,
"product_id" int8 NOT NULL,
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of merchant_product
-- ----------------------------

-- ----------------------------
-- Table structure for payment_method
-- ----------------------------
DROP TABLE IF EXISTS "public"."payment_method";
CREATE TABLE "public"."payment_method" (
"id" int8 DEFAULT nextval('payment_method_id_seq'::regclass) NOT NULL,
"name" varchar(100) COLLATE "default" NOT NULL,
"api_url" varchar(255) COLLATE "default",
"api_key" varchar(255) COLLATE "default",
"media_id" int8,
"enabled" bool NOT NULL,
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of payment_method
-- ----------------------------

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS "public"."product";
CREATE TABLE "public"."product" (
"id" int8 DEFAULT nextval('product_id_seq'::regclass) NOT NULL,
"sku" varchar(20) COLLATE "default" NOT NULL,
"upc" varchar(20) COLLATE "default",
"ean" varchar(20) COLLATE "default",
"name" varchar(100) COLLATE "default" NOT NULL,
"description" text COLLATE "default",
"product_category_id" int8 NOT NULL,
"brand_id" int8 NOT NULL,
"merchant_id" int8 NOT NULL,
"price" numeric(19,5) NOT NULL,
"qty_available" numeric(19,5) NOT NULL,
"qty_sell" numeric(19,5) NOT NULL,
"status" varchar(45) COLLATE "default" NOT NULL,
"enabled" bool NOT NULL,
"slug" varchar(255) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of product
-- ----------------------------

-- ----------------------------
-- Table structure for product_attribute
-- ----------------------------
DROP TABLE IF EXISTS "public"."product_attribute";
CREATE TABLE "public"."product_attribute" (
"id" int8 DEFAULT nextval('product_attribute_id_seq'::regclass) NOT NULL,
"product_id" int8 NOT NULL,
"name" varchar(255) COLLATE "default" NOT NULL,
"value" text COLLATE "default",
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of product_attribute
-- ----------------------------

-- ----------------------------
-- Table structure for product_category
-- ----------------------------
DROP TABLE IF EXISTS "public"."product_category";
CREATE TABLE "public"."product_category" (
"id" int8 DEFAULT nextval('product_category_id_seq'::regclass) NOT NULL,
"name" varchar(45) COLLATE "default" NOT NULL,
"media_id" int8,
"parent_id" int8,
"sequence" int8 NOT NULL,
"description" text COLLATE "default",
"enabled" bool NOT NULL,
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of product_category
-- ----------------------------

-- ----------------------------
-- Table structure for product_category_attribute
-- ----------------------------
DROP TABLE IF EXISTS "public"."product_category_attribute";
CREATE TABLE "public"."product_category_attribute" (
"id" int8 DEFAULT nextval('product_category_attribute_id_seq'::regclass) NOT NULL,
"product_category_id" int8 NOT NULL,
"name" varchar(255) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of product_category_attribute
-- ----------------------------

-- ----------------------------
-- Table structure for product_image
-- ----------------------------
DROP TABLE IF EXISTS "public"."product_image";
CREATE TABLE "public"."product_image" (
"id" int8 DEFAULT nextval('product_image_id_seq'::regclass) NOT NULL,
"product_id" int8 NOT NULL,
"media_id" int8,
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of product_image
-- ----------------------------

-- ----------------------------
-- Table structure for product_meta
-- ----------------------------
DROP TABLE IF EXISTS "public"."product_meta";
CREATE TABLE "public"."product_meta" (
"id" int8 DEFAULT nextval('product_meta_id_seq'::regclass) NOT NULL,
"product_id" int8 NOT NULL,
"meta_key" varchar(255) COLLATE "default",
"meta_value" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of product_meta
-- ----------------------------

-- ----------------------------
-- Table structure for promo
-- ----------------------------
DROP TABLE IF EXISTS "public"."promo";
CREATE TABLE "public"."promo" (
"id" int8 DEFAULT nextval('promo_id_seq'::regclass) NOT NULL,
"code" varchar(45) COLLATE "default",
"name" varchar(100) COLLATE "default",
"valid_from" date,
"valid_to" date,
"type" int2,
"value" numeric(19,5),
"minimum_purchase" numeric(19,5),
"description" text COLLATE "default",
"media_id" int8,
"primary" bool,
"enabled" bool,
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of promo
-- ----------------------------

-- ----------------------------
-- Table structure for province
-- ----------------------------
DROP TABLE IF EXISTS "public"."province";
CREATE TABLE "public"."province" (
"id" int8 DEFAULT nextval('province_id_seq'::regclass) NOT NULL,
"name" varchar(100) COLLATE "default" NOT NULL,
"country_id" int8 NOT NULL,
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of province
-- ----------------------------

-- ----------------------------
-- Table structure for system_language
-- ----------------------------
DROP TABLE IF EXISTS "public"."system_language";
CREATE TABLE "public"."system_language" (
"id" int8 DEFAULT nextval('system_language_id_seq'::regclass) NOT NULL,
"label" varchar(60) COLLATE "default" NOT NULL,
"country_id" int8 NOT NULL,
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of system_language
-- ----------------------------

-- ----------------------------
-- Table structure for system_language_list
-- ----------------------------
DROP TABLE IF EXISTS "public"."system_language_list";
CREATE TABLE "public"."system_language_list" (
"id" int8 DEFAULT nextval('system_language_list_id_seq'::regclass) NOT NULL,
"system_language_id" int8 NOT NULL,
"origin_text" varchar(255) COLLATE "default" NOT NULL,
"change_text" varchar(255) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of system_language_list
-- ----------------------------

-- ----------------------------
-- Table structure for system_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."system_log";
CREATE TABLE "public"."system_log" (
"id" int8 DEFAULT nextval('system_log_id_seq'::regclass) NOT NULL,
"system_user_id" int8,
"log_date" timestamp(6),
"description" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of system_log
-- ----------------------------

-- ----------------------------
-- Table structure for system_setting
-- ----------------------------
DROP TABLE IF EXISTS "public"."system_setting";
CREATE TABLE "public"."system_setting" (
"id" int8 DEFAULT nextval('system_setting_id_seq'::regclass) NOT NULL,
"meta_key" varchar(255) COLLATE "default" NOT NULL,
"meta_value" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of system_setting
-- ----------------------------

-- ----------------------------
-- Table structure for system_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."system_user";
CREATE TABLE "public"."system_user" (
"id" int8 DEFAULT nextval('system_user_id_seq'::regclass) NOT NULL,
"username" varchar(100) COLLATE "default" NOT NULL,
"username_canonical" varchar(100) COLLATE "default" NOT NULL,
"display_name" varchar(100) COLLATE "default" NOT NULL,
"email" varchar(100) COLLATE "default" NOT NULL,
"email_canonical" varchar(100) COLLATE "default" NOT NULL,
"enabled" bool NOT NULL,
"password" varchar(255) COLLATE "default" NOT NULL,
"last_login" timestamp(6),
"confirmation_token" varchar(180) COLLATE "default",
"confirmation_otp" int4,
"password_request_at" timestamp(6),
"roles" text COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"created_by" varchar(45) COLLATE "default",
"updated_at" timestamp(6),
"updated_by" varchar(45) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of system_user
-- ----------------------------

-- ----------------------------
-- Table structure for system_user_meta
-- ----------------------------
DROP TABLE IF EXISTS "public"."system_user_meta";
CREATE TABLE "public"."system_user_meta" (
"id" int8 DEFAULT nextval('system_user_meta_id_seq'::regclass) NOT NULL,
"system_user_id" int8 NOT NULL,
"meta_key" varchar(255) COLLATE "default",
"meta_value" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of system_user_meta
-- ----------------------------

-- ----------------------------
-- Table structure for table1
-- ----------------------------
DROP TABLE IF EXISTS "public"."table1";
CREATE TABLE "public"."table1" (
"field1" varchar(255) COLLATE "default",
"field2" varchar(255) COLLATE "default",
"id" int4 NOT NULL,
"age" int4,
"createdAt" timestamp(6),
"updatedAt" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of table1
-- ----------------------------
INSERT INTO "public"."table1" VALUES ('Hallo', 'Apa kabar', '1', '6', '2018-08-17 03:16:28', '2018-08-06 03:17:11');
INSERT INTO "public"."table1" VALUES ('OK', 'Kabar baik', '2', '5', '2018-08-17 03:16:34', '2018-08-27 03:17:16');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
ALTER SEQUENCE "public"."bank_account_id_seq" OWNED BY "bank_account"."id";
ALTER SEQUENCE "public"."bank_id_seq" OWNED BY "bank"."id";
ALTER SEQUENCE "public"."brand_id_seq" OWNED BY "brand"."id";
ALTER SEQUENCE "public"."city_id_seq" OWNED BY "city"."id";
ALTER SEQUENCE "public"."color_id_seq" OWNED BY "color"."id";
ALTER SEQUENCE "public"."country_id_seq" OWNED BY "country"."id";
ALTER SEQUENCE "public"."coupon_id_seq" OWNED BY "coupon"."id";
ALTER SEQUENCE "public"."courier_id_seq" OWNED BY "courier"."id";
ALTER SEQUENCE "public"."courier_service_id_seq" OWNED BY "courier_service"."id";
ALTER SEQUENCE "public"."currency_id_seq" OWNED BY "currency"."id";
ALTER SEQUENCE "public"."customer_address_id_seq" OWNED BY "customer_address"."id";
ALTER SEQUENCE "public"."customer_bank_id_seq" OWNED BY "customer_bank"."id";
ALTER SEQUENCE "public"."customer_card_id_seq" OWNED BY "customer_card"."id";
ALTER SEQUENCE "public"."customer_id_seq" OWNED BY "customer"."id";
ALTER SEQUENCE "public"."customer_meta_id_seq" OWNED BY "customer_meta"."id";
ALTER SEQUENCE "public"."media_id_seq" OWNED BY "media"."id";
ALTER SEQUENCE "public"."media_meta_id_seq" OWNED BY "media_meta"."id";
ALTER SEQUENCE "public"."merchant_id_seq" OWNED BY "merchant"."id";
ALTER SEQUENCE "public"."merchant_meta_id_seq" OWNED BY "merchant_meta"."id";
ALTER SEQUENCE "public"."merchant_product_id_seq" OWNED BY "merchant_product"."id";
ALTER SEQUENCE "public"."payment_method_id_seq" OWNED BY "payment_method"."id";
ALTER SEQUENCE "public"."product_attribute_id_seq" OWNED BY "product_attribute"."id";
ALTER SEQUENCE "public"."product_category_attribute_id_seq" OWNED BY "product_category_attribute"."id";
ALTER SEQUENCE "public"."product_category_id_seq" OWNED BY "product_category"."id";
ALTER SEQUENCE "public"."product_id_seq" OWNED BY "product"."id";
ALTER SEQUENCE "public"."product_image_id_seq" OWNED BY "product_image"."id";
ALTER SEQUENCE "public"."product_meta_id_seq" OWNED BY "product_meta"."id";
ALTER SEQUENCE "public"."promo_id_seq" OWNED BY "promo"."id";
ALTER SEQUENCE "public"."province_id_seq" OWNED BY "province"."id";
ALTER SEQUENCE "public"."system_language_id_seq" OWNED BY "system_language"."id";
ALTER SEQUENCE "public"."system_language_list_id_seq" OWNED BY "system_language_list"."id";
ALTER SEQUENCE "public"."system_log_id_seq" OWNED BY "system_log"."id";
ALTER SEQUENCE "public"."system_setting_id_seq" OWNED BY "system_setting"."id";
ALTER SEQUENCE "public"."system_user_id_seq" OWNED BY "system_user"."id";
ALTER SEQUENCE "public"."system_user_meta_id_seq" OWNED BY "system_user_meta"."id";

-- ----------------------------
-- Indexes structure for table bank
-- ----------------------------
CREATE INDEX "idx_60673_fk_bank_country1_idx" ON "public"."bank" USING btree (country_id);
CREATE INDEX "idx_60673_fk_bank_media1_idx" ON "public"."bank" USING btree (media_id);

-- ----------------------------
-- Primary Key structure for table bank
-- ----------------------------
ALTER TABLE "public"."bank" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table bank_account
-- ----------------------------
CREATE INDEX "idx_60682_fk_bank_account_bank1_idx" ON "public"."bank_account" USING btree (bank_id);

-- ----------------------------
-- Primary Key structure for table bank_account
-- ----------------------------
ALTER TABLE "public"."bank_account" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table brand
-- ----------------------------
ALTER TABLE "public"."brand" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table city
-- ----------------------------
CREATE INDEX "idx_60709_fk_city_province1_idx" ON "public"."city" USING btree (province_id);

-- ----------------------------
-- Primary Key structure for table city
-- ----------------------------
ALTER TABLE "public"."city" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table color
-- ----------------------------
ALTER TABLE "public"."color" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table country
-- ----------------------------
ALTER TABLE "public"."country" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table coupon
-- ----------------------------
CREATE INDEX "idx_60727_fk_coupon_media1_idx" ON "public"."coupon" USING btree (media_id);

-- ----------------------------
-- Primary Key structure for table coupon
-- ----------------------------
ALTER TABLE "public"."coupon" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table courier
-- ----------------------------
CREATE INDEX "idx_60733_fk_courier_media1_idx" ON "public"."courier" USING btree (media_id);

-- ----------------------------
-- Primary Key structure for table courier
-- ----------------------------
ALTER TABLE "public"."courier" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table courier_service
-- ----------------------------
CREATE INDEX "idx_60742_fk_courier_service_courier1_idx" ON "public"."courier_service" USING btree (courier_id);

-- ----------------------------
-- Primary Key structure for table courier_service
-- ----------------------------
ALTER TABLE "public"."courier_service" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table currency
-- ----------------------------
ALTER TABLE "public"."currency" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table customer
-- ----------------------------
CREATE INDEX "idx_60754_fk_customer_media1_idx" ON "public"."customer" USING btree (media_id);
CREATE INDEX "idx_60754_fk_customer_system_user1_idx" ON "public"."customer" USING btree (system_user_id);

-- ----------------------------
-- Primary Key structure for table customer
-- ----------------------------
ALTER TABLE "public"."customer" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table customer_address
-- ----------------------------
CREATE INDEX "idx_60760_fk_customer_address_city1_idx" ON "public"."customer_address" USING btree (city_id);
CREATE INDEX "idx_60760_fk_customer_address_country1_idx" ON "public"."customer_address" USING btree (country_id);
CREATE INDEX "idx_60760_fk_customer_address_customer1_idx" ON "public"."customer_address" USING btree (customer_id);
CREATE INDEX "idx_60760_fk_customer_address_province1_idx" ON "public"."customer_address" USING btree (province_id);

-- ----------------------------
-- Primary Key structure for table customer_address
-- ----------------------------
ALTER TABLE "public"."customer_address" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table customer_bank
-- ----------------------------
CREATE INDEX "idx_60769_fk_customer_bank_bank1_idx" ON "public"."customer_bank" USING btree (bank_id);
CREATE INDEX "idx_60769_fk_customer_bank_customer1_idx" ON "public"."customer_bank" USING btree (customer_id);

-- ----------------------------
-- Primary Key structure for table customer_bank
-- ----------------------------
ALTER TABLE "public"."customer_bank" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table customer_card
-- ----------------------------
CREATE INDEX "idx_60775_fk_customer_payment_customer1_idx" ON "public"."customer_card" USING btree (customer_id);

-- ----------------------------
-- Primary Key structure for table customer_card
-- ----------------------------
ALTER TABLE "public"."customer_card" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table customer_meta
-- ----------------------------
CREATE INDEX "idx_60793_fk_customer_meta_customer1_idx" ON "public"."customer_meta" USING btree (customer_id);

-- ----------------------------
-- Primary Key structure for table customer_meta
-- ----------------------------
ALTER TABLE "public"."customer_meta" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table media
-- ----------------------------
CREATE INDEX "idx_60889_fk_media_system_user1_idx" ON "public"."media" USING btree (system_user_id);

-- ----------------------------
-- Primary Key structure for table media
-- ----------------------------
ALTER TABLE "public"."media" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table media_meta
-- ----------------------------
CREATE INDEX "idx_60898_fk_media_meta_media1_idx" ON "public"."media_meta" USING btree (media_id);

-- ----------------------------
-- Primary Key structure for table media_meta
-- ----------------------------
ALTER TABLE "public"."media_meta" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table merchant
-- ----------------------------
CREATE INDEX "idx_60907_fk_merchant_city1_idx" ON "public"."merchant" USING btree (city_id);
CREATE INDEX "idx_60907_fk_merchant_country1_idx" ON "public"."merchant" USING btree (country_id);
CREATE INDEX "idx_60907_fk_merchant_media1_idx" ON "public"."merchant" USING btree (media_id);
CREATE INDEX "idx_60907_fk_merchant_province1_idx" ON "public"."merchant" USING btree (province_id);
CREATE INDEX "idx_60907_fk_merchant_system_user1_idx" ON "public"."merchant" USING btree (system_user_id);

-- ----------------------------
-- Primary Key structure for table merchant
-- ----------------------------
ALTER TABLE "public"."merchant" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table merchant_meta
-- ----------------------------
CREATE INDEX "idx_60970_fk_merchant_meta_merchant1_idx" ON "public"."merchant_meta" USING btree (merchant_id);

-- ----------------------------
-- Primary Key structure for table merchant_meta
-- ----------------------------
ALTER TABLE "public"."merchant_meta" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table merchant_product
-- ----------------------------
CREATE INDEX "idx_60964_fk_merchant_product_merchant1_idx" ON "public"."merchant_product" USING btree (merchant_id);
CREATE INDEX "idx_60964_fk_merchant_product_merchant_storage1_idx" ON "public"."merchant_product" USING btree (merchant_storage_id);
CREATE INDEX "idx_60964_fk_merchant_product_product1_idx" ON "public"."merchant_product" USING btree (product_id);

-- ----------------------------
-- Primary Key structure for table merchant_product
-- ----------------------------
ALTER TABLE "public"."merchant_product" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table payment_method
-- ----------------------------
CREATE INDEX "idx_61042_fk_payment_method_media1_idx" ON "public"."payment_method" USING btree (media_id);

-- ----------------------------
-- Primary Key structure for table payment_method
-- ----------------------------
ALTER TABLE "public"."payment_method" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table product
-- ----------------------------
CREATE UNIQUE INDEX "idx_60808_ean_unique" ON "public"."product" USING btree (ean);
CREATE INDEX "idx_60808_fk_product_brand1_idx" ON "public"."product" USING btree (brand_id);
CREATE INDEX "idx_60808_fk_product_merchant1_idx" ON "public"."product" USING btree (merchant_id);
CREATE INDEX "idx_60808_fk_product_product_category1_idx" ON "public"."product" USING btree (product_category_id);
CREATE UNIQUE INDEX "idx_60808_sku_unique" ON "public"."product" USING btree (sku);
CREATE UNIQUE INDEX "idx_60808_upc_unique" ON "public"."product" USING btree (upc);

-- ----------------------------
-- Primary Key structure for table product
-- ----------------------------
ALTER TABLE "public"."product" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table product_attribute
-- ----------------------------
CREATE INDEX "idx_60817_fk_product_attribute_product1_idx" ON "public"."product_attribute" USING btree (product_id);

-- ----------------------------
-- Primary Key structure for table product_attribute
-- ----------------------------
ALTER TABLE "public"."product_attribute" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table product_category
-- ----------------------------
CREATE INDEX "idx_60826_fk_product_category_media_idx" ON "public"."product_category" USING btree (media_id);
CREATE INDEX "idx_60826_fk_product_category_product_category1_idx" ON "public"."product_category" USING btree (parent_id);

-- ----------------------------
-- Primary Key structure for table product_category
-- ----------------------------
ALTER TABLE "public"."product_category" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table product_category_attribute
-- ----------------------------
CREATE INDEX "idx_60835_fk_product_category_atrribute_product_category1_idx" ON "public"."product_category_attribute" USING btree (product_category_id);

-- ----------------------------
-- Primary Key structure for table product_category_attribute
-- ----------------------------
ALTER TABLE "public"."product_category_attribute" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table product_image
-- ----------------------------
CREATE INDEX "idx_60841_fk_product_image_media1_idx" ON "public"."product_image" USING btree (media_id);
CREATE INDEX "idx_60841_fk_product_image_product1_idx" ON "public"."product_image" USING btree (product_id);

-- ----------------------------
-- Primary Key structure for table product_image
-- ----------------------------
ALTER TABLE "public"."product_image" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table product_meta
-- ----------------------------
CREATE INDEX "idx_60853_fk_product_meta_product1_idx" ON "public"."product_meta" USING btree (product_id);

-- ----------------------------
-- Primary Key structure for table product_meta
-- ----------------------------
ALTER TABLE "public"."product_meta" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table promo
-- ----------------------------
ALTER TABLE "public"."promo" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table province
-- ----------------------------
CREATE INDEX "idx_61051_fk_province_country1_idx" ON "public"."province" USING btree (country_id);

-- ----------------------------
-- Primary Key structure for table province
-- ----------------------------
ALTER TABLE "public"."province" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table system_language
-- ----------------------------
CREATE INDEX "idx_61066_fk_system_language_country1_idx" ON "public"."system_language" USING btree (country_id);

-- ----------------------------
-- Primary Key structure for table system_language
-- ----------------------------
ALTER TABLE "public"."system_language" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table system_language_list
-- ----------------------------
CREATE INDEX "idx_61072_fk_system_language_list_system_language1_idx" ON "public"."system_language_list" USING btree (system_language_id);

-- ----------------------------
-- Primary Key structure for table system_language_list
-- ----------------------------
ALTER TABLE "public"."system_language_list" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table system_log
-- ----------------------------
CREATE INDEX "idx_61081_fk_system_log_system_user1_idx" ON "public"."system_log" USING btree (system_user_id);

-- ----------------------------
-- Primary Key structure for table system_log
-- ----------------------------
ALTER TABLE "public"."system_log" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table system_setting
-- ----------------------------
ALTER TABLE "public"."system_setting" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table system_user
-- ----------------------------
ALTER TABLE "public"."system_user" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table system_user_meta
-- ----------------------------
CREATE INDEX "idx_61108_fk_system_user_meta_system_user1_idx" ON "public"."system_user_meta" USING btree (system_user_id);

-- ----------------------------
-- Primary Key structure for table system_user_meta
-- ----------------------------
ALTER TABLE "public"."system_user_meta" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table table1
-- ----------------------------
ALTER TABLE "public"."table1" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Key structure for table "public"."bank"
-- ----------------------------
ALTER TABLE "public"."bank" ADD FOREIGN KEY ("country_id") REFERENCES "public"."country" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "public"."bank" ADD FOREIGN KEY ("media_id") REFERENCES "public"."media" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."bank_account"
-- ----------------------------
ALTER TABLE "public"."bank_account" ADD FOREIGN KEY ("bank_id") REFERENCES "public"."bank" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."city"
-- ----------------------------
ALTER TABLE "public"."city" ADD FOREIGN KEY ("province_id") REFERENCES "public"."province" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."coupon"
-- ----------------------------
ALTER TABLE "public"."coupon" ADD FOREIGN KEY ("media_id") REFERENCES "public"."media" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."courier"
-- ----------------------------
ALTER TABLE "public"."courier" ADD FOREIGN KEY ("media_id") REFERENCES "public"."media" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."courier_service"
-- ----------------------------
ALTER TABLE "public"."courier_service" ADD FOREIGN KEY ("courier_id") REFERENCES "public"."courier" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."customer"
-- ----------------------------
ALTER TABLE "public"."customer" ADD FOREIGN KEY ("media_id") REFERENCES "public"."media" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."customer" ADD FOREIGN KEY ("system_user_id") REFERENCES "public"."system_user" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."customer_address"
-- ----------------------------
ALTER TABLE "public"."customer_address" ADD FOREIGN KEY ("country_id") REFERENCES "public"."country" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "public"."customer_address" ADD FOREIGN KEY ("city_id") REFERENCES "public"."city" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "public"."customer_address" ADD FOREIGN KEY ("province_id") REFERENCES "public"."province" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "public"."customer_address" ADD FOREIGN KEY ("customer_id") REFERENCES "public"."customer" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."customer_bank"
-- ----------------------------
ALTER TABLE "public"."customer_bank" ADD FOREIGN KEY ("customer_id") REFERENCES "public"."customer" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "public"."customer_bank" ADD FOREIGN KEY ("bank_id") REFERENCES "public"."bank" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."customer_card"
-- ----------------------------
ALTER TABLE "public"."customer_card" ADD FOREIGN KEY ("customer_id") REFERENCES "public"."customer" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."customer_meta"
-- ----------------------------
ALTER TABLE "public"."customer_meta" ADD FOREIGN KEY ("customer_id") REFERENCES "public"."customer" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."media"
-- ----------------------------
ALTER TABLE "public"."media" ADD FOREIGN KEY ("system_user_id") REFERENCES "public"."system_user" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."media_meta"
-- ----------------------------
ALTER TABLE "public"."media_meta" ADD FOREIGN KEY ("media_id") REFERENCES "public"."media" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."merchant"
-- ----------------------------
ALTER TABLE "public"."merchant" ADD FOREIGN KEY ("country_id") REFERENCES "public"."country" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "public"."merchant" ADD FOREIGN KEY ("system_user_id") REFERENCES "public"."system_user" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "public"."merchant" ADD FOREIGN KEY ("province_id") REFERENCES "public"."province" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "public"."merchant" ADD FOREIGN KEY ("city_id") REFERENCES "public"."city" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "public"."merchant" ADD FOREIGN KEY ("media_id") REFERENCES "public"."media" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."merchant_meta"
-- ----------------------------
ALTER TABLE "public"."merchant_meta" ADD FOREIGN KEY ("merchant_id") REFERENCES "public"."merchant" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."merchant_product"
-- ----------------------------
ALTER TABLE "public"."merchant_product" ADD FOREIGN KEY ("merchant_id") REFERENCES "public"."merchant" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "public"."merchant_product" ADD FOREIGN KEY ("product_id") REFERENCES "public"."product" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."payment_method"
-- ----------------------------
ALTER TABLE "public"."payment_method" ADD FOREIGN KEY ("media_id") REFERENCES "public"."media" ("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."product"
-- ----------------------------
ALTER TABLE "public"."product" ADD FOREIGN KEY ("product_category_id") REFERENCES "public"."product_category" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "public"."product" ADD FOREIGN KEY ("merchant_id") REFERENCES "public"."merchant" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "public"."product" ADD FOREIGN KEY ("brand_id") REFERENCES "public"."brand" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."product_attribute"
-- ----------------------------
ALTER TABLE "public"."product_attribute" ADD FOREIGN KEY ("product_id") REFERENCES "public"."product" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."product_category"
-- ----------------------------
ALTER TABLE "public"."product_category" ADD FOREIGN KEY ("media_id") REFERENCES "public"."media" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."product_category" ADD FOREIGN KEY ("parent_id") REFERENCES "public"."product_category" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."product_category_attribute"
-- ----------------------------
ALTER TABLE "public"."product_category_attribute" ADD FOREIGN KEY ("product_category_id") REFERENCES "public"."product_category" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."product_image"
-- ----------------------------
ALTER TABLE "public"."product_image" ADD FOREIGN KEY ("media_id") REFERENCES "public"."media" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "public"."product_image" ADD FOREIGN KEY ("product_id") REFERENCES "public"."product" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."product_meta"
-- ----------------------------
ALTER TABLE "public"."product_meta" ADD FOREIGN KEY ("product_id") REFERENCES "public"."product" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."promo"
-- ----------------------------
ALTER TABLE "public"."promo" ADD FOREIGN KEY ("media_id") REFERENCES "public"."media" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."province"
-- ----------------------------
ALTER TABLE "public"."province" ADD FOREIGN KEY ("country_id") REFERENCES "public"."country" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."system_language"
-- ----------------------------
ALTER TABLE "public"."system_language" ADD FOREIGN KEY ("country_id") REFERENCES "public"."country" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."system_language_list"
-- ----------------------------
ALTER TABLE "public"."system_language_list" ADD FOREIGN KEY ("system_language_id") REFERENCES "public"."system_language" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."system_log"
-- ----------------------------
ALTER TABLE "public"."system_log" ADD FOREIGN KEY ("system_user_id") REFERENCES "public"."system_user" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."system_user_meta"
-- ----------------------------
ALTER TABLE "public"."system_user_meta" ADD FOREIGN KEY ("system_user_id") REFERENCES "public"."system_user" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;
